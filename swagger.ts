import swaggerAutogen from 'swagger-autogen';

const outputFile = './swagger_output.json';
const endpointsFiles = ['./src/routes/v1/index.ts'];

swaggerAutogen(outputFile, endpointsFiles);
