import path from 'path';
import bodyParser from 'body-parser';
import cors from 'cors';
import express, { Application } from 'express';
import routes from './src/routes/v1/index';
import connectDB from './src/config/db';
import { errorHandler } from './src/middlewares/errorHandler';
import setHeaders from './src/middlewares/setheaders';
import SwaggerUI from 'swagger-ui-express';
import * as fs from 'fs';

const app: Application = express();
const port = process.env.PORT || 5000;
connectDB();
// Set Headers middleware
app.use(setHeaders);

// Cors Middleware
app.use(cors({ origin: '*' }));

// Middleware
app.use(bodyParser.json({ limit: '25mb' }));
app.use(bodyParser.urlencoded({ extended: false, limit: '25mb' }));

// Load Swagger file
const swaggerFilePath = path.resolve(__dirname, 'swagger_output.json');
let swaggerFile = {};

try {
  const rawData = fs.readFileSync(swaggerFilePath, 'utf-8');
  swaggerFile = JSON.parse(rawData);
} catch (error) {
  console.error('Error reading or parsing Swagger file:', error);
}

// Serve Swagger UI
app.use('/doc', SwaggerUI.serve, SwaggerUI.setup(swaggerFile));

// Routes
app.use('/', routes);

// Error handling middleware
app.use(errorHandler);

// Start server
app.listen(port as number, '0.0.0.0', () => console.log(`Server is started on port ${port}`));
