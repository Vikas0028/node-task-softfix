
# Getting started



This repository contains the backend code for node softfix task.

## Run Locally

Clone the project


Go to the project directory

```bash
cd node
```

# Install dependencies

```bash
yarn run dev
```

Runs the app in the development mode using nodemon & ts-node.<br />
Open http://localhost:5000 to view it in the browser.


