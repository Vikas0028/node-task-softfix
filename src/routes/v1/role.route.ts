import { Router } from "express";
import roleController from "../../controllers/role.controller";
import checkAdmin from "../../middlewares/checkAdmin";
import { authenticate } from "../../auth/jwt.auth";
const router = Router();

router.get("/", authenticate, checkAdmin, roleController.getAllRoles);
router.get("/:roleId", authenticate, checkAdmin, roleController.getRoleById);
router.post("/", authenticate, checkAdmin, roleController.createRole);
router.patch("/:roleId", authenticate, checkAdmin, roleController.updateRole);
router.delete("/:roleId", authenticate, checkAdmin, roleController.deleteRole);

export { router as roleRoute };
