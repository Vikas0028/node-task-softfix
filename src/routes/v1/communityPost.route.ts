import { Router } from "express";
import CommunityPostController from "../../controllers/communityPost.controller";
import { authenticate } from "../../auth/jwt.auth";

const router = Router();

router.post("/",authenticate, CommunityPostController.createCommunityPost);
router.get("/",authenticate, CommunityPostController.getAllCommunityPosts);
router.get("/:postId", authenticate,CommunityPostController.getCommunityPostById);
router.put("/:postId", authenticate,CommunityPostController.updateCommunityPost);
router.delete("/:postId",authenticate, CommunityPostController.deleteCommunityPost);

export { router as communityPostRoute };
