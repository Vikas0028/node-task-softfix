import { Router } from "express";
import PermissionController from "../../controllers/permission.controller";
import checkAdmin from "../../middlewares/checkAdmin";
import { authenticate } from "../../auth/jwt.auth";

const router = Router();
router.get("/",authenticate, checkAdmin, PermissionController.getAllPermissions);
router.post("/", authenticate,checkAdmin, PermissionController.createPermission);
router.put("/:permissionId",authenticate, checkAdmin, PermissionController.updatePermission);
router.delete(
  "/:permissionId",
  authenticate,
  checkAdmin,
  PermissionController.deletePermission
);

export { router as permissionRoute };
