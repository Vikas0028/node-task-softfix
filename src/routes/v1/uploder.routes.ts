import { Router } from 'express';
import { uploadImage } from '../../controllers/uploader.controller';
import { uploadImageMiddleware } from '../../middlewares/uploader.middleware';

const router = Router();

router.post('/img', uploadImageMiddleware, uploadImage);

export { router as uploadFileRoute };
