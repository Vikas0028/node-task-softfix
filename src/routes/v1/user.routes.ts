import { Router } from "express";
import userController from "../../controllers/users.controller";
import checkSuperAdmin from "../../middlewares/checkSuperAdmin";
import { authenticate } from "../../auth/jwt.auth";
const router = Router();

router.post("/register", userController.registerUser);

router.post("/login", userController.loginUser);
router.post(
  "/:userId/assign-role",
  authenticate,
  checkSuperAdmin,
  userController.assignRoleToUser
);
router.post(
  "/:userId/remove-role",
  authenticate,
  checkSuperAdmin,
  userController.removeRoleFromUser
);

// Add route for creating a super admin and apply the checkSuperAdmin middleware
router.post('/create-super-admin', userController.createSuperAdmin);

export { router as userRoute };
