import  express  from "express";
import { userRoute } from "./user.routes";
import { roleRoute } from "./role.route";
import { permissionRoute } from "./permission.route";
import { uploadFileRoute } from "./uploder.routes";
import { communityPostRoute } from "./communityPost.route";

const routes = express.Router()
routes.use('/user', userRoute)
routes.use('/role',roleRoute )
routes.use('/permission', permissionRoute)
routes.use('/upload',uploadFileRoute )
routes.use('/post',communityPostRoute )

export default routes;