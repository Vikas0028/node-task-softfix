import { NextFunction, Request, Response } from "express";
import roleServices from "../services/role.services";

class roleController {
  static async getAllRoles(req: Request, res: Response, next: NextFunction) {
    try {
      const roles = roleServices.getAllRoles();
      res.status(200).json(roles);
    } catch (error) {
      next(error);
    }
  }
  static async getRoleById(req: Request, res: Response, next: NextFunction) {
    try {
      const { roleId } = req.params;
      const role = roleServices.getRoleById(roleId);
      if (role) {
        res.status(200).json(role);
      } else {
        res.status(404).json({ message: "Role not found" });
      }
    } catch (error) {
      next(error);
    }
  }
  static async createRole(req: Request, res: Response, next: NextFunction) {
    try {
      const { name, permissions } = req.body;
      const role = roleServices.createRole({ name, permissions });
      res.status(201).json(role);
    } catch (error) {
      next(error);
    }
  }
  static async updateRole(req: Request, res: Response, next: NextFunction) {
    try {
      const { roleId } = req.params;
      const { name, permissions } = req.body;
      const updatedRole = roleServices.updateRole(roleId, {
        name,
        permissions,
      });
      if (updatedRole) {
        res.status(200).json(updatedRole);
      } else {
        res.status(404).json({ message: "Role not found" });
      }
    } catch (error) {
      next(error);
    }
  }
  static async deleteRole(req: Request, res: Response, next: NextFunction) {
    try {
      await roleServices.deleteRole(req.params.id);
      return res.sendStatus(204);
    } catch (error) {
      next(error);
    }
  }
}

export default roleController;
