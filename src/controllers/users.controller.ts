import { NextFunction, Request, Response } from "express";
import UserServices from "../services/user.services";
import bcrypt from "bcrypt";
import { generateToken } from "../auth/jwt.auth";
class userController {
  static async createSuperAdmin(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      const superAdmin = await UserServices.getUserByUserName("superadmin");
      if (!superAdmin) {
        await UserServices.createSuperAdmin();
        res.status(201).json({ msg: "Super admin created successfully" });
      } else {
        res.status(403).send({
          msg: "Super admin already exists.",
          status: "failed",
        });
      }
    } catch (error) {
      next(error);
    }
  }
  static async registerUser(req: Request, res: Response, next: NextFunction) {
    try {
      const { username, email, password } = req?.body;
      const saltRounds = 10;
      const hashedPassword = password
        ? await bcrypt.hash(password, saltRounds)
        : "";
      const data = {
        userName: username,
        email: email,
        password: hashedPassword,
      };
      const user = await UserServices.getUserByUserName(username);
      if (!user) {
        const savedUser = await UserServices.createUser(data);
        res.status(201).json({ msg: "User Created" });
      } else {
        res.status(403).send({
          msg: "User already Exist with this Username",
          status: "failed",
        });
      }
    } catch (error) {
      next(error);
    }
  }
  static async loginUser(req: Request, res: Response, next: NextFunction) {
    try {
      const userName = req.body.username;
      const inputPassword = req.body.password;
      let user;
      if (!req.body.username) {
        return res
          .status(400)
          .json({ message: "Username is required.", status: "success" });
      } else {
        user = await UserServices.getUserByUserName(userName);
      }
      if (user) {
        const token = generateToken({
          userId: user._id.toString(),
          roles: user.roles,
        });
        const hashedPassword = user.password;
        const isMatch = await bcrypt.compare(inputPassword, hashedPassword);
        if (isMatch) {
          return res.status(200).json({ user: user, token: token });
        } else {
          return res
            .status(400)
            .json({ message: "Invalid password.", status: "success" });
        }
      } else {
        return res
          .status(400)
          .json({ message: "User does not exist.", status: "success" });
      }
    } catch (error) {
      next(error);
    }
  }
  static async assignRoleToUser(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { userId } = req.params;
      const { roleName } = req.body;
      const updatedUser = await UserServices.assignRoleToUser(userId, roleName);
      if (updatedUser) {
        res.status(200).json(updatedUser);
      } else {
        res.status(404).json({ message: "User not found" });
      }
    } catch (error) {
      next(error);
    }
  }

  static async removeRoleFromUser(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { userId } = req.params;
      const { roleName } = req.body;
      const updatedUser = await UserServices.removeRoleFromUser(
        userId,
        roleName
      );
      if (updatedUser) {
        res.status(200).json(updatedUser);
      } else {
        res.status(404).json({ message: "User not found" });
      }
    } catch (error) {
      next(error);
    }
  }
}

export default userController;
