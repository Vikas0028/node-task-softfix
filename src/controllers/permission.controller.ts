import { NextFunction, Request, Response } from "express";
import permissionServices from "../services/permission.services";

class PermissionController {
  static async getAllPermissions(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      const permissions = permissionServices.getAllPermissions();
      res.status(200).json(permissions);
    } catch (error) {
      next(error);
    }
  }
  static async createPermission(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { name } = req.body;
      const permission = permissionServices.createPermission({ name });
      res.status(201).json(permission);
    } catch (error) {
      next(error);
    }
  }
  static async updatePermission(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { permissionId } = req.params;
      const { name } = req.body;
      const updatedPermission = permissionServices.updatePermission(
        permissionId,
        { name }
      );
      if (updatedPermission) {
        res.status(200).json(updatedPermission);
      } else {
        res.status(404).json({ message: "Permission not found" });
      }
    } catch (error) {
      next(error);
    }
  }
  static async deletePermission(req: Request, res: Response, next: NextFunction) {
    try {
      await permissionServices.deletePermission(req.params.id);
      return res.sendStatus(204);
    } catch (error) {
      next(error);
    }
  }
}

export default PermissionController;
