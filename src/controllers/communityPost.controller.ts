import { NextFunction, Request, Response } from "express";
import CommunityPostServices from "../services/communityPost.services";

class CommunityPostController {
  static async createCommunityPost(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      const postData = req.body;
      const newPost = await CommunityPostServices.createPost(postData);
      res.status(201).json(newPost);
    } catch (error) {
      next(error);
    }
  }

  static async getAllCommunityPosts(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      const posts = await CommunityPostServices.getAllPosts();
      res.status(200).json(posts);
    } catch (error) {
      next(error);
    }
  }

  static async getCommunityPostById(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      const postId = req.params.postId;
      const post = await CommunityPostServices.getPostById(postId);
      if (post) {
        res.status(200).json(post);
      } else {
        res.status(404).json({ message: "Community post not found" });
      }
    } catch (error) {
      next(error);
    }
  }

  static async updateCommunityPost(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      const postId = req.params.postId;
      const postData = req.body;
      const updatedPost = await CommunityPostServices.updatePost(
        postId,
        postData
      );
      if (updatedPost) {
        res.status(200).json(updatedPost);
      } else {
        res.status(404).json({ message: "Community post not found" });
      }
    } catch (error) {
      next(error);
    }
  }

  static async deleteCommunityPost(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      const postId = req.params.postId;
      await CommunityPostServices.deletePost(postId);
      res.sendStatus(204);
    } catch (error) {
      next(error);
    }
  }
}

export default CommunityPostController;
