import { NextFunction, Request, Response } from 'express';
import { UploadedFile } from 'express-fileupload';
import fs from 'fs';
import path from 'path';

export const uploadImage = async (req: Request, res: Response, next: NextFunction) => {
   try {
      const file = req.file as unknown as UploadedFile & { buffer: Buffer };
      if (!file) throw new Error('No file uploaded');
      const { buffer, mimetype } = file;
      let isImage = mimetype.startsWith('image/');
      let isGif = mimetype === 'image/gif';

      if (!isImage && !isGif) throw new Error('Invalid file type');

      const imgType = req.query.imgType;
      let directoryPath = '';

      if (isGif) {
         directoryPath = path.join(__dirname, '..', '../public/gifs');
      } else {
         if (imgType === 'users') {
            directoryPath = path.join(__dirname, '..', '../public/users');
         } else if (imgType === 'services') {
            directoryPath = path.join(__dirname, '..', '../public/services');
         } else if (imgType === 'products') {
            directoryPath = path.join(__dirname, '..', '../public/products');
         } else if (imgType === 'gallery') {
            directoryPath = path.join(__dirname, '..', '../public/gallery');
         } else if (imgType === 'workshop') {
            directoryPath = path.join(__dirname, '..', '../public/workshop');
         } else if (imgType === 'img') {
            directoryPath = path.join(__dirname, '..', '../public/img');
         }
      }

      const extension = isGif ? '.gif' : `.${mimetype.split('/')[1]}`;
      const filename = `${Date.now()}${extension}`;
      const filepath = path.join(directoryPath, filename);

      // Create directory if it doesn't exist
      if (!fs.existsSync(directoryPath)) {
         fs.mkdirSync(directoryPath, { recursive: true });
      }

      fs.writeFileSync(filepath, buffer);

      return res.status(201).json({
         message: 'Image uploaded successfully',
         imgUrl: `/${isGif ? 'gifs' : imgType}/${filename}`,
      });
   } catch (err) {
      next(err);
   }
};
