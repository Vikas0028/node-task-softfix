import { NextFunction, Request, Response } from 'express';

const checkSuperAdmin = (req: Request, res: Response, next: NextFunction) => {
  console.log(req,"reqqqqqqqqqqqqqq")
  const userRoles = (req.user && req.user.roles) || [];
  const isSuperAdmin = userRoles.includes('superadmin');

  if (isSuperAdmin) {
    next();
  } else {
    res.status(403).json({ message: 'Permission denied. Super-admin access required.', status: 'failed' });
  }
};

export default checkSuperAdmin;
