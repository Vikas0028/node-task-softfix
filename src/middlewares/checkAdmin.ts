import { NextFunction, Request, Response } from "express";

const checkAdmin = (req: Request, res: Response, next: NextFunction) => {
  const userRoles = (req.user && req.user.roles) || [];
  const isAdmin = userRoles.includes("admin");
  if (isAdmin) {
    next();
  } else {
    res.status(403).json({
      message: "Permission denied. Admin access required.",
      status: "failed",
    });
  }
};

export default checkAdmin;
