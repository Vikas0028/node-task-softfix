import mongoose from "mongoose";

const dbConnection = async () => {
  try {
    const MONGO_URI = "mongodb://localhost:27017/test";
    const conn = await mongoose.connect(MONGO_URI);
    console.log(`MongoDB Connected with:`, conn.connection.db.namespace);
  } catch (error: any) {
    console.error(`Error: ${error.message}`);
    process.exit(1);
  }
};

export default dbConnection;
