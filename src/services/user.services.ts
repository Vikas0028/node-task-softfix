import { UserModel } from "../models/users.model";
import bcrypt from "bcrypt";

class UserServices {
  static async getAllUser() {
    return await UserModel.find().exec();
  }

  static async getUserById(id: string) {
    return await UserModel.findById(id).exec();
  }
  static async getUserByUserName(userName: string) {
    return await UserModel.findOne({ userName: userName }).exec();
  }
  static async createUser(userData: any) {
    const newUser = new UserModel(userData);
    return await newUser.save();
  }

  static async updateUser(id: string, userData: any) {
    return await UserModel.findByIdAndUpdate(id, userData, {
      new: true,
      runValidators: true,
    }).exec();
  }

  static async deleteUser(id: string) {
    await UserModel.findByIdAndDelete(id).exec();
  }
  static async assignRoleToUser(userId: string, roleName: string) {
    const user = await UserModel.findById(userId).exec();
    if (user) {
      user.roles.push(roleName);
      return await user.save();
    }
    return null;
  }

  static async removeRoleFromUser(userId: string, roleName: string) {
    const user = await UserModel.findById(userId).exec();
    if (user) {
      user.roles = user.roles.filter((role) => role !== roleName);
      return await user.save();
    }
    return null;
  }

  static async createSuperAdmin() {
    const superAdminData = {
      userName: "superadmin",
      email: "superadmin@example.com",
      password: await bcrypt.hash("superadminpassword", 10),
      roles: ["superadmin"],
    };

    return await UserModel.create(superAdminData);
  }

}
export default UserServices;
