import { roleModel } from "../models/role.model";
class roleServices {
  static async getAllRoles() {
    return await roleModel.find().exec();
  }

  static async getRoleById(id: string) {
    return await roleModel.findById(id).exec();
  }

  static async createRole(roleData: any) {
    const newRole = new roleModel(roleData);
    return await newRole.save();
  }

  static async updateRole(id: string, roleData: any) {
    return await roleModel.findByIdAndUpdate(id, roleData, {
      new: true,
      runValidators: true,
    }).exec();
  }

  static async deleteRole(id: string) {
    await roleModel.findByIdAndDelete(id).exec();
  }
}
export default roleServices;
