import CommunityPostModel from '../models/communityPost.model';

class CommunityServices {
  static async getAllPosts() {
    return await CommunityPostModel.find().exec();
  }

  static async getPostById(id: string) {
    return await CommunityPostModel.findById(id).exec();
  }

  static async createPost(postData: any) {
    const newPost = new CommunityPostModel(postData);
    return await newPost.save();
  }

  static async updatePost(id: string, postData: any) {
    return await CommunityPostModel.findByIdAndUpdate(id, postData, {
      new: true,
      runValidators: true,
    }).exec();
  }

  static async deletePost(id: string) {
    await CommunityPostModel.findByIdAndDelete(id).exec();
  }
}

export default CommunityServices;
