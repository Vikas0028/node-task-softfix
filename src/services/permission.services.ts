import { PermissionModel } from "../models/permission.model";
class permissionServices {
  static async getAllPermissions() {
    return await PermissionModel.find().exec();
  }

  static async getPermissionById(id: string) {
    return await PermissionModel.findById(id).exec();
  }

  static async createPermission(permissionData: any) {
    const newPermission = new PermissionModel(permissionData);
    return await newPermission.save();
  }

  static async updatePermission(id: string, permissionData: any) {
    return await PermissionModel.findByIdAndUpdate(id, permissionData, {
      new: true,
      runValidators: true,
    }).exec();
  }

  static async deletePermission(id: string) {
    await PermissionModel.findByIdAndDelete(id).exec();
  }
}
export default permissionServices;
