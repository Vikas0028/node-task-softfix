import jwt from 'jsonwebtoken';
import { Request, Response, NextFunction } from 'express';
declare module 'express' {
  interface Request {
    user?: {
      userId: string;
      roles: string[];
    };
  }
}

const JWT_SECRET = 'hjgsdjhfdbncvhjcdn76437843bhdgb';

export function generateToken(payload: { userId: string; roles: string[] }): string {
  const token = jwt.sign(payload, JWT_SECRET, { expiresIn: '7d' });
  return token;
}

export function verifyToken(token: string): { userId: string; roles: string[] } {
  const decoded = jwt.verify(token, JWT_SECRET) as { userId: string; roles: string[] };
  return decoded;
}

export function authenticate(req: Request, res: Response, next: NextFunction): void {
  const authHeader = req.headers.authorization;

  if (authHeader && authHeader.startsWith('Bearer ')) {
    const token = authHeader.substring(7);

    try {
      const payload = verifyToken(token);
      console.log(payload,"ssssssssssssss")
      req.user = payload;
      next();
    } catch (error) {
      res.status(401).json({
        message: 'Invalid or expired token',
        status: 'error',
      });
    }
  } else {
    res.status(401).json({
      message: 'Authentication required',
      status: 'error',
    });
  }
}
