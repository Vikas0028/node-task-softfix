import mongoose, { Document, Schema } from "mongoose";
export interface Permissions extends Document {
  id: string;
  name: string;
}

const PermissionsSchema: Schema = new Schema({
  name: {
    type: String,
  },
});

export const PermissionModel = mongoose.model<Permissions>(
  "Permissions",
  PermissionsSchema
);
