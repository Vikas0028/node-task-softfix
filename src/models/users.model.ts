import mongoose, { Document, Schema } from "mongoose";
export interface Users extends Document {
  id: string;
  userName: string;
  email: string;
  password: string;
  roles: string[];
}

const userSchema: Schema = new Schema({
  userName: {
    type: String,
  },
  email: {
    type: String,
  },
  password: {
    type: String,
  },
  roles: {
    type: [],
    ref: "Role",
  },
});

export const UserModel = mongoose.model<Users>("User", userSchema);
