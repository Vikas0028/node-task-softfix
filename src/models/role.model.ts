import mongoose, { Document, Schema } from "mongoose";
export interface Role extends Document {
  id: string;
  name: string;
  permissions: string[];
}

const RoleSchema: Schema = new Schema({
  name: {
    type: String,
  },
  permissions: {
    type: [],
    ref:'Permissions'
  },
});

export const roleModel = mongoose.model<Role>(
  "Role",
  RoleSchema
);
