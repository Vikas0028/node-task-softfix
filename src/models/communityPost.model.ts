import mongoose, { Schema, Document } from "mongoose";

export interface ICommunityPost extends Document {
  title: string;
  type: string;
  content: string;
  author: string;
}

const CommunityPostSchema: Schema = new Schema({
  title: { type: String },
  type: {
    type: String,
    enum: ["ANSWER", "QUERY"],
  },
  content: { type: String },
  author: { type: String },
});

const CommunityPostModel = mongoose.model<ICommunityPost>(
  "CommunityPost",
  CommunityPostSchema
);

export default CommunityPostModel;
